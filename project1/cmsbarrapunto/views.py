from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from cmsbarrapunto.models import Page
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
import urllib.request


formulario = """
<form action="" method="POST">
    <h1>Introducir contenido de la pagina o actualizar: </h1><br>
    <input type='text' name='content' value=''>
    <input type='submit' value='Enviar'></form>
"""
html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"
global titulares

def parsear(url):
    global lista
    lista =""
    class myContentHandler(ContentHandler):

        def __init__ (self):
            self.inItem = False
            self.inContent = False
            self.theContent = ""

        def startElement (self, name, attrs):
            if name == 'item':
                self.inItem = True
            elif self.inItem:
                if name == 'title':
                    self.inContent = True
                elif name == 'link':
                    self.inContent = True

        def endElement (self, name):
            global lista
            if name == 'item':
                self.inItem = False
            elif self.inItem:
                if name == 'title':
                    self.title = self.theContent
                    lista += '<p>Title: ' + self.title + '.<br></p>'
                    self.inContent = False
                    self.theContent = ""
                elif name == 'link':
                    self.link = self.theContent
                    lista += '<p>Link: ' + '<a href='+ self.link + '>'+ self.title + '</a></p>'
                    self.inContent = False
                    self.theContent = ""
                    self.title = ""
                    self.link = ""

        def characters (self, chars):
            if self.inContent:
                self.theContent = self.theContent + chars

    theParser = make_parser()
    theHandler = myContentHandler()
    theParser.setContentHandler(theHandler)

    xmlFile = urllib.request.urlopen(url)
    theParser.parse(xmlFile)
    return lista

titulares ="<br> TITULARES: <br>"
titulares += parsear("http://barrapunto.com/index.rss");

def show_list(request):

    list_pages = Page.objects.all()
    if (len(list_pages) == 0):
        body = "Aun no hay paginas almacenadas"
    else:
        body = "Lista de paginas almacenadas: "
        for p in list_pages:
            print(p.name)
            body += html_item_template.format(name=p.name)
        body += lista
        body += "</ul>"
    return(HttpResponse(html_template.format(body=body)))

@csrf_exempt
def show_or_add(request, name):

    if request.method == 'GET':
        try:
            p = Page.objects.get(name=name)
            content = p.content
            response = HttpResponse("El contenido de la pagina es " + content + formulario + lista)
        except Page.DoesNotExist:
            response = HttpResponse(("Pagina " + name + " no encontrada. Rellenar formulario") + formulario + lista)
    elif request.method == 'POST':
        try:
            p = Page.objects.get(name=name)
            p.content = request.POST['content'] #actualizo contenido
        except Page.DoesNotExist:
            p = Page(name=name,content = request.POST['content'])
        p.save()
        content = p.content
        response = HttpResponse(content + formulario + lista)
    else:
        response = HttpResponseNotFound("ERROR")


    return(response)
